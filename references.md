# References

[1] Roberto Morabito, Jimmy Kjällman, and Miika Komu. "Hypervisors vs. Lightweight Virtualization: a Performance Comparison." 2015 IEEE International Conference on Cloud Engineering.

[2] Richard W.M. Jones Red Hat Inc. "Optimizing QEMU Boot Time."