# Abstract

Virtualization is a technology that allows us to run various kinds of mechanisms in the cloud. Virtualization technology promises to be able to provide superior performance compared to other technologies. The definitions and uses of virtualization technology will be expounded upon in this report.

Various comparison tools have been used to understand the advantages and disadvantages of this technology in various ways in terms of memory, speed, processing, storage, and configuration. From the final results obtained, conclusions have been drawn that the Container offers better performance in terms of the aspects mentioned earlier. Although containers offer more benefits than other technologies, the difference between Containers and other technologies is not significant.

The general belief is that containers are really fast and lightweight, whereas full virtualization is time consuming and resource intense, which is generally true until Intel demonstrated full Linux virtual machines booting as fast as containers and using as little memory. 

Intel’s work used *kvmtool*, a loadable kernel module, that provides the core virtualization infrastructure and a processor specific module so that one can run multiple virtual machines running unmodified Linux or Windows images, and a customized, cut down guest kernel. 

For a Windows image, since it cannot be modified, we are limited in the approaches we can take to only focusing on modifying the hypervisor. Even if one never plans to use containers, or use Windows as a mobile application, one may still benefit from a faster QEMU experience.

In later years, during the advent of mobile computing, when tablets became widespread, Samsung’s Research and Development team was tasked with the problem of running Windows 8.1 VM on Android as an application. To the question whether this be accomplished using general distributions of GNU/Linux through libvirt, QEMU, SeaBIOS, based on previous research, the short answer is no. However, we can still achieve very fast boot times. 