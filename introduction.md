# Introduction

Virtualization technology has played a very dominant role in recent years, and the amount of software that utilizes containers has increased over time. One reason is to speed up the exchange of information between users and servers.

At the same time, this technology has expanded to a variety of different scopes, and is used by almost all circles in everyday life. The use of Virtualization in contexts such as the Cloud Environment, Internet of Things, and the Network Function Environment is broader. The main benefits of Virtualization are increased hardware independence, isolation, increased scalability and security. Therefore, Virtualization is very attractive and competitive. He also contributed greatly to improving new solutions. Furthermore, many attempts to encourage the development of hybrid techniques that promise to combine the advantages that existed before.

In this article, we will also show performance analysis using different comparison tools. The aim is to calculate the amount of overhead and gaps that exist compared to other technologies.

## background

In this section, we provide an overview of the differences between Container-based Virtualization and Hypervisor Virtualization. We also want to show the difference betwee a container and a highly customized virtual machine.

Container-based Virtualization provides a different level of abstraction in terms of virtualization and isolation compared to other hypervisors. The container can be said to be the right fast alternative compared to hypervisor-based virtualization.

Hypervisor Virtualization has been used by various groups since the last few decades. Hypervisors operate at the hardware level by being independent and separate from the host system. Therefore, the Hypervisor can run the Windows operating system. Whereas, containers cannot. However, the Container size is much smaller than the Hypervisor, because the operating system like Windows has a large size and the emulation of the virtual hardware device incurs more overhead.

There are two types of Hypervisor Virtualization, which are *native or bare-metal hypervisor* and *hosted hypervisor*. *Native or bare-metal hypervisor* operates over the hardware, whereas *hosted hypervisor* operates over the host's operating system. However, the difference between these two types of hypervisors are not that significant. For instance, Linux Kernel-based Virtual Machine (KVM) has two of those characteristics. Therefore, we only focus on KVM.

Almost all hardware seperates the hypervisors into seperate virtual layers. Therefore it causes an increase in overhead. Operating systems like Linux usually run on this virtualized hardware in each virtual machine. Instead, containers implement isolation processes at the operating system level, thus avoiding excessive overhead. In addition, containers can be executed in an operating system, in the same kernel on a host machine. These containers can run the software that users want individually.

The advantage of Container-based virtualization is that they can generate as many agencies as many as we want. The size of each container is certainly much smaller than the usual hypervisor. The shared kernel approach also has several disadvantages. One container limit is a container with a Windows operating system that cannot be run on a Linux host. "As another tradeoff, containers do not isolate resources and hypervisors because host kernels are exposed to containers, which can be a problem for multi-tenant security."

As another tradeoff, containers do not isolate resources as well as hypervisors because the host kernel is exposed to the containers, which can be an issue for multi-tenant security.

## Motivation

A program should start instantly. “5 seconds is the max time most user will wait for a website or application to load. 74% will leave a mobile website if it doesn’t load within 5 seconds, and 50% will exit an app.” Although this is not necessarily true for desktop applications, we must acknowledge the fact that users want their programs to load as quickly as possible, which is the problem we are faced with. What we want to do is to boot up and shut down a modern Operating System (Microsoft Windows and GNU/Linux) in a KVM-enabled virtual machine on a modern Linux host. For this project, we will be working to build a secure container runtime with lightweight virtual machines that feel and perform like containers, but provide stronger workload isolation using hardware virtualization technology with a concentration on the overhead of the boot and shutdown.