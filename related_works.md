# Related Works

Numerous attempts have been made in order to optimize in hope for decreasing the performance gap between virtualized and non-virtualized machines. The investigation methods and the technologies presented vary for each paper. 

Roberto et all [1] compared four different platforms, which are KVM, LXC, Docker, and OSv in terms of CPU, Memory, Disk I/O, and Network I/O performance. In their research, they concluded that the KVM performance has significantly increased over the past few years. However, the Disk I/O efficiency still represents as an obstacle for certain types of applications, although deeper evaluation of Disk I/O is still needed because some incompatibility between different tools.

The overhead produced by containers be completely ignored. Considering the differences between a Docker and a highly customized KVM, it's clear that Docker performs much better although it's versatile and easy to configure yet insecure. 

While in Richard W.M. Jones [2] research paper, he concluded that reduce the boot time of the QEMU from 3.5 seconds down to 1.2 seconds. 